#Random Weapons for Maogata Outcasts
#May, 30. 2010
#by shoggi

from CvPythonExtensions import *

def onUnitCreated(self, argsList):
	'Unit Completed'
	unit = argsList[0]
	gc 	= CyGlobalContext()
	pPlayer = gc.getPlayer(unit.getOwner())

	if pPlayer.getCivilizationType() == gc.getInfoTypeForString('CIVILIZATION_MAOGATA'):
		if unit.getUnitClassType() == gc.getInfoTypeForString('UNITCLASS_OUTCAST'):
			iWeaponRnd = CyGame().getSorenRandNum(100, "Choose your Weapon")
			iBonus = CyGame().getSorenRandNum(100, "Moar goodies for outcasts")
			iReligion = CyGame().getSorenRandNum(9, "Thats me in the corner")
			iReligion2 = CyGame().getSorenRandNum(100, "Thats me in the spot-light")
			if iWeaponRnd < 35:
				unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_BRONZE_WEAPONS'), True)
			elif iWeaponRnd < 55:
				unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_POISONED_BLADE'), True)
			elif iWeaponRnd < 65:
				unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_SPIRITUAL_HAMMER'), True)	
			elif iWeaponRnd < 95:
				unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_IRON_WEAPONS'), True)
			elif iWeaponRnd < 100:
				unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_MITHRIL_WEAPONS'), True)
			if unit.isHasPromotion(gc.getInfoTypeForString('PROMOTION_SPIRITUAL_HAMMER')):
				unit.setReligion(iReligion)
				if iReligion2 < 50:
					unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_SHIELD_OF_FAITH'), True)
			if unit.isHasPromotion(gc.getInfoTypeForString('PROMOTION_SPIRITUAL_HAMMER')) == false:
				if iBonus < 20:
					unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_ENCHANTED_BLADE'), True)
				elif iBonus < 30:
					unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_CURSED_WEAPONS_MELEE'), True)
				elif iBonus < 50:
					unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_FINE_EQUIPMENT_MELEE'), True)
				elif iBonus < 55:
					unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_RUSTED'), True)
				elif iBonus < 56:
					unit.setHasPromotion(gc.getInfoTypeForString('PROMOTION_HERO'), True)
					
def onBeginPlayerTurn(self, argsList):
	iGameTurn, iPlayer = argsList
	gc 				= CyGlobalContext() 
	game 			= CyGame()
	eSpeed 			= game.getGameSpeedType()
	Speed			= self.GameSpeeds
	getPlayer 		= gc.getPlayer
	pPlayer 		= getPlayer(iPlayer)
	pCity 			= pPlayer.getCapitalCity()
	
	
	
	if pPlayer.getCivilizationType() == gc.getInfoTypeForString('CIVILIZATION_MAOGATA'):
		if pPlayer.isFeatAccomplished(FeatTypes.FEAT_GLOBAL_SPELL):
			iCycle = 50
			if   eSpeed == Speed["Quick"]: 	  iCycle = 33
			elif eSpeed == Speed["Epic"]: 	  iCycle = 75
			elif eSpeed == Speed["Marathon"]: iCycle = 100
			for i in xrange(10):
				if (i * iCycle) - 5 == iGameTurn:
					pPlayer.setFeatAccomplished(FeatTypes.FEAT_GLOBAL_SPELL, False)
					CyInterface().addMessage(iPlayer,true,25,CyTranslator().getText("TXT_KEY_MESSAGE_WORLDSPELL_AVAILABLE", ()),'',1,'Art/Civs/Maogata/buttons/promo_cajar.dds',ColorTypes(8),pCity.getX(),pCity.getY(),True,True)
		