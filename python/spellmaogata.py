#maogata spells
#may, 30, 2010
#by shogg

from CvPythonExtensions import *
import PyHelpers
gc = CyGlobalContext()
PyPlayer = PyHelpers.PyPlayer
RANGE2 	= ((-1,-2),(-1,-1),(-1,0),(-1,1),(-1,2),(0,-2),(0,-1),(0,0),(0,1),(0,2),(1,-2),(1,-1),(1,0),(1,1),(1,2),(-2,-2),(-2,-1),(-2,0),(-2,1),(-2,2),(2,-2),(2,-1),(2,0),(2,1),(2,2))
	

def reqFreeMagic(caster):
	pPlot = caster.plot()
	iImprovement = pPlot.getImprovementType()
	getInfoType = gc.getInfoTypeForString
	iNode = [getInfoType('IMPROVEMENT_MANA_DIMENSIONAL'), getInfoType('IMPROVEMENT_MANA_AIR'), getInfoType('IMPROVEMENT_MANA_BODY'), getInfoType('IMPROVEMENT_MANA_CHAOS'), getInfoType('IMPROVEMENT_MANA_DEATH'), getInfoType('IMPROVEMENT_MANA_EARTH'), getInfoType('IMPROVEMENT_MANA_ENCHANTMENT'), getInfoType('IMPROVEMENT_MANA_ENTROPY'), getInfoType('IMPROVEMENT_MANA_FIRE'), getInfoType('IMPROVEMENT_MANA_FORCE'), getInfoType('IMPROVEMENT_MANA_ICE'), getInfoType('IMPROVEMENT_MANA_LAW'), getInfoType('IMPROVEMENT_MANA_LIFE'), getInfoType('IMPROVEMENT_MANA_METAMAGIC'), getInfoType('IMPROVEMENT_MANA_MIND'), getInfoType('IMPROVEMENT_MANA_NATURE'), getInfoType('IMPROVEMENT_MANA_SHADOW'), getInfoType('IMPROVEMENT_MANA_SPIRIT'), getInfoType('IMPROVEMENT_MANA_SUN'), getInfoType('IMPROVEMENT_MANA_WATER'), getInfoType('IMPROVEMENT_MANA_CREATION')]
	if pPlot.getOwner() == caster.getOwner():
		if pPlot.getBonusType(-1) != -1:
			if gc.getBonusInfo(pPlot.getBonusType(-1)).getBonusClassType() == gc.getInfoTypeForString('BONUSCLASS_MANA'):
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_AIR') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_AIR')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_EARTH') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_EARTH')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_FIRE') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_FIRE')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_ICE') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_ICE')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_WATER') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_WATER')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_LAW') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_LAW')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_LIFE') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_LIFE')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_MIND') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_MIND')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_SPIRIT') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_SPIRIT')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_SUN') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_SUN')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_BODY') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_BODY')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_CREATION') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_CREATION')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_ENCHANTMENT') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_ENCHANTMENT')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_FORCE') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_FORCE')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_NATURE') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_NATURE')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_CHAOS') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_CHAOS')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_DEATH') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_DEATH')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_DIMENSIONAL') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_DIMENSION')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_ENTROPY') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_ENTROPY')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_SHADOW') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_SHADOW')) == True:
					return False
				if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_METAMAGIC') and caster.isHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_METAMAGIC')) == True:
					return False
				if iImprovement in iNode:
					return True
		return False

def spellFreeMagic(caster):
	pPlot = caster.plot()
	if pPlot.getBonusType(-1) != -1:
		if gc.getBonusInfo(pPlot.getBonusType(-1)).getBonusClassType() == gc.getInfoTypeForString('BONUSCLASS_MANA'):
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_AIR'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_AIR'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_EARTH'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_EARTH'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_FIRE'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_FIRE'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_ICE'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_ICE'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_WATER'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_WATER'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_LAW'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_LAW'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_LIFE'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_LIFE'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_MIND'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_MIND'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_SPIRIT'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_SPIRIT'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_SUN'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_SUN'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_BODY'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_BODY'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_CREATIOM'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_CREATION'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_ENCHANTMENT'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_ENCHANTMENT'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_FORCE'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_FORCE'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_NATURE'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_NATURE'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_CHAOS'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_CHAOS'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_DEATH'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_DEATH'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_DIMENSIONAL'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_DIMENSION'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_ENTROPY'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_ENTROPY'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_SHADOW'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_SHADOW'), True)
			if pPlot.getBonusType(-1) == gc.getInfoTypeForString('BONUS_MANA_METAMAGIC'):
				caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_INFUSION_METAMAGIC'), True)
			else:
				if gc.getImprovementInfo(pPlot.getImprovementType()).isPermanent() == False:
					pPlot.setImprovementType(-1)
					pPlot.setBonusType(-1)			
			CyInterface().addMessage(caster.getOwner(),true,25,CyTranslator().getText("TXT_KEY_MESSAGE_MANA_INFUSION", ()),'',1,'Modules/NormalModules/Maogata/Art/Civs/Maogata/buttons/voices.dds',ColorTypes(8),pPlot.getX(),pPlot.getY(),True,True)
		
def spellGreatHunt(caster):
	iPrey = gc.getInfoTypeForString('PROMOTION_PREY')
	pPlayer = gc.getPlayer(caster.getOwner())
	iTeam = pPlayer.getTeam()
	py = PyPlayer(caster.getOwner())
	pOrc = PyPlayer(gc.getORC_PLAYER())
	pDemon = PyPlayer(gc.getDEMON_PLAYER())
	pAnimal = PyPlayer(gc.getANIMAL_PLAYER())
	def cleanListFromPrey( inList ):
		return [ prey for prey in inList if not prey.isHasPromotion(iPrey) ]
	preyList = cleanListFromPrey( pOrc.getUnitList() + pDemon.getUnitList() + pAnimal.getUnitList() )
	for pUnit in py.getUnitList():
		if preyList:
			pTargetUnit = preyList.pop( CyGame().getSorenRandNum( len(preyList), "target prey") )
			pTargetUnit.setHasPromotion(iPrey, True)
			pPlot = pTargetUnit.plot()
			pPlot.setRevealed(iTeam, True, False, TeamTypes.NO_TEAM)
			CyInterface().addMessage(caster.getOwner(),true,25,CyTranslator().getText("TXT_KEY_MESSAGE_FIND_PREY", ()),'',1,'Modules/NormalModules/Maogata/Art/Civs/Maogata/buttons/promo_prey.dds',ColorTypes(8),pTargetUnit.getX(),pTargetUnit.getY(),True,True)
		else:
			break
				
def postCombatPreyKill(pCaster, pOpponent):
	pMaogata = gc.getPlayer(pOpponent.getOwner())
	if pMaogata.getCivilizationType() == gc.getInfoTypeForString('CIVILIZATION_MAOGATA'):
		pOpponent.setHasPromotion(gc.getInfoTypeForString('PROMOTION_CAJAR'), True)	
	
def reqChearsulMana(caster):
	pPlot = caster.plot()
	if pPlot.getOwner() == caster.getOwner():
		if pPlot.getBonusType(-1) != -1:
			if gc.getBonusInfo(pPlot.getBonusType(-1)).getBonusClassType() == gc.getInfoTypeForString('BONUSCLASS_MANA'):
				if pPlot.getImprovementType() == -1:
					if pPlot.getImprovementType() == gc.getInfoTypeForString('IMPROVEMENT_MANA_WATER') or pPlot.getImprovementType() == gc.getInfoTypeForString('IMPROVEMENT_MANA_ICE'):
						return True
		return False
		
def spellChearsulMana(caster):
	caster.setHasPromotion(gc.getInfoTypeForString('PROMOTION_ICEWATER_INFUSION'), True)
	pPlot.setImprovementType(-1)
	pPlot.setBonusType(-1)	

def spellAbsorbMagicLow(caster):
	pPlot = caster.plot()
	iX = pPlot.getX()
	iY = pPlot.getY()
	getPlot	= CyMap().plot
	if gc.getGame().isOption(GameOptionTypes.GAMEOPTION_NO_PLOT_COUNTER):
		iBrokenLands = gc.getInfoTypeForString('TERRAIN_BROKEN_LANDS')
		iBurningSands = gc.getInfoTypeForString('TERRAIN_BURNING_SANDS')
		iDesert = gc.getInfoTypeForString('TERRAIN_DESERT')
		iFieldsOfPerdition = gc.getInfoTypeForString('TERRAIN_FIELDS_OF_PERDITION')
		iGrass = gc.getInfoTypeForString('TERRAIN_GRASS')
		iMarsh = gc.getInfoTypeForString('TERRAIN_MARSH')
		iPlains = gc.getInfoTypeForString('TERRAIN_PLAINS')
		iShallows = gc.getInfoTypeForString('TERRAIN_SHALLOWS')
		iTerrain = pPlot.getTerrainType()
		if iTerrain == iBrokenLands:
			pPlot.setTerrainType(iGrass, False, False)
		if iTerrain == iBurningSands:
			pPlot.setTerrainType(iDesert, False, False)
		if iTerrain == iFieldsOfPerdition:
			pPlot.setTerrainType(iPlains, False, False)
		if iTerrain == iShallows:
			pPlot.setTerrainType(iMarsh, False, False)
	else:
		if pPlot.getPlotCounter() > 0:
			pPlot.changePlotCounter(pPlot.getPlotCounter() * -1)

		rebuildGraphics()
	
def spellAbsorbMagicHigh(caster):
	pPlot = caster.plot()
	iX = pPlot.getX()
	iY = pPlot.getY()
	getPlot	= CyMap().plot
	if gc.getGame().isOption(GameOptionTypes.GAMEOPTION_NO_PLOT_COUNTER):
		iBrokenLands = gc.getInfoTypeForString('TERRAIN_BROKEN_LANDS')
		iBurningSands = gc.getInfoTypeForString('TERRAIN_BURNING_SANDS')
		iDesert = gc.getInfoTypeForString('TERRAIN_DESERT')
		iFieldsOfPerdition = gc.getInfoTypeForString('TERRAIN_FIELDS_OF_PERDITION')
		iGrass = gc.getInfoTypeForString('TERRAIN_GRASS')
		iMarsh = gc.getInfoTypeForString('TERRAIN_MARSH')
		iPlains = gc.getInfoTypeForString('TERRAIN_PLAINS')
		iShallows = gc.getInfoTypeForString('TERRAIN_SHALLOWS')
		for iiX,iiY in RANGE1:
			pPlot = getPlot(iX+iiX,iY+iiY)
			if not pPlot.isNone():
				iTerrain = pPlot.getTerrainType()
				if iTerrain == iBrokenLands:
					pPlot.setTerrainType(iGrass, False, False)
				if iTerrain == iBurningSands:
					pPlot.setTerrainType(iDesert, False, False)
				if iTerrain == iFieldsOfPerdition:
					pPlot.setTerrainType(iPlains, False, False)
				if iTerrain == iShallows:
					pPlot.setTerrainType(iMarsh, False, False)
	else:
		for iiX,iiY in RANGE1:
			pPlot = getPlot(iX+iiX,iY+iiY)
			if pPlot.getPlotCounter() > 0:
				pPlot.changePlotCounter(pPlot.getPlotCounter() * -1)

		rebuildGraphics()

def reqPlantForest(caster):
	pPlot = caster.plot()
	iFeature = pPlot.getFeatureType()
	iImprovement = pPlot.getImprovementType()
	if iFeature == -1 and iImprovement == -1:
		return true
	return false
	
def reqFindPrey(caster):
	pPlayer = gc.getPlayer(caster.getOwner())
	if pPlayer.isFeatAccomplished(FeatTypes.FEAT_GLOBAL_SPELL):
		return true
	return false
	
def spellFindPrey(caster):
	iPrey = gc.getInfoTypeForString('PROMOTION_PREY')
	pPlot = caster.plot()
	for iPlayer in range(gc.getMAX_PLAYERS()):
		pPlayer = gc.getPlayer(iPlayer)
		py = PyPlayer(iPlayer)
		for pUnit in py.getUnitList():
			if pUnit.isHasPromotion(iPrey):
				CyInterface().addMessage(caster.getOwner(),true,25,CyTranslator().getText("TXT_KEY_MESSAGE_FIND_PREY", ()),'',1,'Modules/NormalModules/Maogata/Art/Civs/Maogata/buttons/promo_prey.dds',ColorTypes(8),pUnit.getX(),pUnit.getY(),True,True)
				
def spellChannelVoices(caster):
	gc			= CyGlobalContext()
	getInfoType = gc.getInfoTypeForString
	ePhysical	= getInfoType('DAMAGE_PHYSICAL')
	effect		= CyEngine().triggerEffect
	iX = caster.getX()
	iY = caster.getY()
	getPlot	= CyMap().plot
	iCount = 0
	iMinDamage = 20
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_AIR')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_EARTH')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_FIRE')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_ICE')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_WATER')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_LAW')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_LIFE')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_MIND')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_SPIRIT')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_SUN')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_BODY')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_CREATION')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_ENCHANTMENT')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_FORCE')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_NATURE')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_CHAOS')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_DEATH')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_DIMENSION')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_ENTROPY')):
		iCount += 1
	if caster.isHasPromotion(getInfoType('PROMOTION_INFUSION_SHADOW')):
		iCount += 1
	iMinDamage += (3 * iCount)
	for iiX,iiY in RANGE2:
		iNewX = iX+iiX; iNewY = iY+iiY
		pPlot = getPlot(iNewX,iNewY)
		if not (iX == iNewX and iY == iNewY):
			for i in xrange(pPlot.getNumUnits()):
				pUnit = pPlot.getUnit(i)
				if not pUnit.getOwner() == caster.getOwner():
					pUnit.setHasPromotion(getInfoType('PROMOTION_INSANE'), True)
					pUnit.doDamage(iMinDamage, 100, caster, ePhysical, True)
			effect(gc.getInfoTypeForString('EFFECT_SPRING'),pPlot.getPoint())

def postCombatWinInfusionCreation(pCaster, pOpponent):
	pPlayer = gc.getPlayer(pCaster.getOwner())
	if CyGame().getSorenRandNum(100, "Birth Chance") < 10:
		pMinPopCity = pPlayer.getCity(1)
		for iCity in xrange(pPlayer.getNumCities()-1):
			pCity = pPlayer.getCity(iCity+1)
			if pMinPopCity.getPopulation() >= pCity.getPopulation():
				pMinPopCity = pCity
		pMinPopCity.changePopulation(1)
				
def postCombatWinInfusionEntropy(pCaster, pOpponent):
	gc			= CyGlobalContext()
	getInfoType = gc.getInfoTypeForString
	iArcane = getInfoType('UNITCOMBAT_ARCANE')
	if pOpponent.getUnitCombatType() == iArcane:
		iBoost = getInfoType('PROMOTION_INFUSION_ENTROPY_BOOST')
		if pCaster.isHasPromotion(iBoost):
			pCaster.setHasPromotion(iBoost, FALSE)
		pCaster.setHasPromotion(iBoost, TRUE)
		
def effectInfusionChaos(caster):
	iStrength = caster.getStrBoost()
	caster.changeStrBoost(-iStrength)
	iNewBoost = CyGame().getSorenRandNum(6, "Random Strength")
	caster.changeStrBoost(iNewBoost - 2)